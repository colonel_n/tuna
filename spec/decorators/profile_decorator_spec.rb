# coding: utf-8
require 'spec_helper'

describe ProfileDecorator do
  let(:profile) { Profile.new.extend ProfileDecorator }
  subject { profile }
  it { should be_a Profile }
end
