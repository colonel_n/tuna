require "spec_helper"

describe AttachFilesController do
  describe "routing" do

    it "routes to #index" do
      get("/attach_files").should route_to("attach_files#index")
    end

    it "routes to #new" do
      get("/attach_files/new").should route_to("attach_files#new")
    end

    it "routes to #show" do
      get("/attach_files/1").should route_to("attach_files#show", :id => "1")
    end

    it "routes to #edit" do
      get("/attach_files/1/edit").should route_to("attach_files#edit", :id => "1")
    end

    it "routes to #create" do
      post("/attach_files").should route_to("attach_files#create")
    end

    it "routes to #update" do
      put("/attach_files/1").should route_to("attach_files#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/attach_files/1").should route_to("attach_files#destroy", :id => "1")
    end

  end
end
