require 'spec_helper'

describe "attach_files/edit" do
  before(:each) do
    @attach_file = assign(:attach_file, stub_model(AttachFile))
  end

  it "renders the edit attach_file form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", attach_file_path(@attach_file), "post" do
    end
  end
end
