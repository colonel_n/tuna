require 'spec_helper'

describe "attach_files/new" do
  before(:each) do
    assign(:attach_file, stub_model(AttachFile).as_new_record)
  end

  it "renders new attach_file form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", attach_files_path, "post" do
    end
  end
end
