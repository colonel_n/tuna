require 'spec_helper'

describe "groups_users/edit" do
  before(:each) do
    @groups_user = assign(:groups_user, stub_model(GroupsUser))
  end

  it "renders the edit groups_user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", groups_user_path(@groups_user), "post" do
    end
  end
end
