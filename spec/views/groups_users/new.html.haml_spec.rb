require 'spec_helper'

describe "groups_users/new" do
  before(:each) do
    assign(:groups_user, stub_model(GroupsUser).as_new_record)
  end

  it "renders new groups_user form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", groups_users_path, "post" do
    end
  end
end
