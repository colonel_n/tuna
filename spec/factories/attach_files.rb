# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :attach_file do
    message_id 1
    file_name "MyString"
    type 1
  end
end
