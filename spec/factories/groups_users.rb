# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :groups_user, :class => 'GroupsUsers' do
    user_id 1
    group_id 1
  end
end
