# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :read_message do
    message_id 1
    user_id 1
  end
end
