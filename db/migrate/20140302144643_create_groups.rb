class CreateGroups < ActiveRecord::Migration
  def change
    create_table :groups do |t|
      t.string :name, :null => false
      t.string :icon, :null => false, :default => ""

      t.timestamps
    end
  end
end
