class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :user_id, :null => false
      t.integer :group_id, :null => false
      t.text :text

      t.timestamps
    end
  end
end
