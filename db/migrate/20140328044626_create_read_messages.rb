class CreateReadMessages < ActiveRecord::Migration
  def change
    create_table :read_messages do |t|
      t.integer :message_id, :null => false
      t.integer :user_id, :null => false

      t.timestamps
    end
    add_index :read_messages, [:message_id, :user_id], :unique => true
  end
end
