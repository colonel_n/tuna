class AddInfoToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :explain, :string, :null => false, :default => ""
    add_column :groups, :group_code, :string, :null => false
    add_column :groups, :created_by, :string, :null => false
  end
end
