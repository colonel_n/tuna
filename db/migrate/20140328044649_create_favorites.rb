class CreateFavorites < ActiveRecord::Migration
  def change
    create_table :favorites do |t|
      t.integer :message_id, :null => false
      t.integer :user_id, :null => false

      t.timestamps
    end
    add_index :favorites, [:message_id, :user_id], :unique => true
  end
end
