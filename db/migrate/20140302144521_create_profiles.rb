class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.integer :user_id, :null => false
      t.string :name, :null => false, :default => ""
      t.string :icon, :null => false, :default => ""

      t.timestamps
    end
  end
end
