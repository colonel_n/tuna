class CreateAttachFiles < ActiveRecord::Migration
  def change
    create_table :attach_files do |t|
      t.integer :message_id, :null => false
      t.string :file_name, :null => false
      t.integer :type, :null => false

      t.timestamps

    end
    add_index :attach_files, :message_id, :unique => true
  end
end
