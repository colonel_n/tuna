# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140328052536) do

  create_table "attach_files", force: true do |t|
    t.integer  "message_id", null: false
    t.string   "file_name",  null: false
    t.integer  "type",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "attach_files", ["message_id"], name: "index_attach_files_on_message_id", unique: true, using: :btree

  create_table "favorites", force: true do |t|
    t.integer  "message_id", null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "favorites", ["message_id", "user_id"], name: "index_favorites_on_message_id_and_user_id", unique: true, using: :btree

  create_table "groups", force: true do |t|
    t.string   "name",                    null: false
    t.string   "icon",       default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "explain",    default: "", null: false
    t.string   "group_code",              null: false
    t.string   "created_by",              null: false
  end

  create_table "groups_users", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "group_id",   null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "messages", force: true do |t|
    t.integer  "user_id",    null: false
    t.integer  "group_id",   null: false
    t.text     "text"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "profiles", force: true do |t|
    t.integer  "user_id",                 null: false
    t.string   "name",       default: "", null: false
    t.string   "icon",       default: "", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "read_messages", force: true do |t|
    t.integer  "message_id", null: false
    t.integer  "user_id",    null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "read_messages", ["message_id", "user_id"], name: "index_read_messages_on_message_id_and_user_id", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
