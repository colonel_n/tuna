PROFILE_ICONS_FULL_PATH = "#{Rails.root}/app/assets/images/profile_icons"
PROFILE_ICONS_MINI_FULL_PATH = "#{PROFILE_ICONS_FULL_PATH}/mini"
GROUP_ICONS_FULL_PATH = "#{Rails.root}/app/assets/images/group_icons"
GROUP_ICONS_MINI_FULL_PATH = "#{GROUP_ICONS_FULL_PATH}/mini"
USER_ICONS_URL = "/assets/profile_icons/normal"
USER_ICONS_MINI_URL = "/assets/profile_icons/mini"
GROUP_ICONS_URL = "/assets/group_icons/normal"
GROUP_ICONS_MINI_URL = "/assets/group_icons/mini"


