# coding: utf-8
module GroupDecorator
  def icon_mini
    "#{GROUP_ICONS_MINI_URL}/#{icon}"
  end

  def owner_icon
    "#{USER_ICONS_MINI_URL}/#{users.find(created_by).profile.icon}"
  end

  def owner_name
    users.find(created_by).profile.name
  end

end
