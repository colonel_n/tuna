# coding: utf-8
module ProfileDecorator
  def display_name
    name.present? ? name : "あの方"
  end

  def image_icon
    image_tag icon? ? "profile_icons/normal/#{icon}" : "profile_icons/normal/agasa.jpg"
  end

end
