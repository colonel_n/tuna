class GroupsController < ApplicationController
  include IconCreator
  before_action :authenticate_user!
  before_action :set_group, only: [:show, :edit, :update, :destroy]

  # GET /groups
  # GET /groups.json
  def index
    @group = Group.new
    @groups = Group.all.order('created_at DESC')
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
  end

  # GET /groups/new
  def new
    @group = Group.new
  end

  # GET /groups/1/edit
  def edit
  end

  # POST /groups
  # POST /groups.json
  def create
    icon = params["icon_img"] ?
        save_resize_icons(GROUP_ICONS_FULL_PATH, params["icon_img"]) : ""
    respond_to do |format|
      if current_user.groups.create group_params.merge({created_by: current_user.id, group_code: make_group_code, icon: icon})
        format.html { redirect_to groups_path, notice: 'Group was successfully created.' }
        #format.json { render action: 'show', status: :created, location: @group }
      else
        format.html { render action: 'new' }
        #format.json { render json: @group.errors, status: :unprocessable_entity }
      end

    end

  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    respond_to do |format|
      if @group.update(group_params)
        format.html { redirect_to @group, notice: 'Group was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @group.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group.destroy
    respond_to do |format|
      format.html { redirect_to groups_url }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = Group.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:group).permit(:name, :explain)
  end

  def make_group_code
    (("a".."z").to_a + ("A".."Z").to_a + (0..9).to_a).shuffle[0..5].join
  end

end
