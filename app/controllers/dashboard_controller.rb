class DashboardController < ApplicationController
  include AccessControl
  before_action :access_deny_before_profile_create
  before_action :authenticate_user!

  def index
    @groups = current_user.groups
  end

end
