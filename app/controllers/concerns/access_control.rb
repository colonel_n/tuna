module AccessControl
  def access_deny_before_profile_create
    redirect_to new_profile_path if current_user && Profile.where(["user_id=?", current_user.id]).size < 1
  end
end