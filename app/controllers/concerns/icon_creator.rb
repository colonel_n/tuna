module IconCreator

  def save_resize_icons(base_path, img)
    icon_name = make_random_file_name img.original_filename
    icon_resize size_info: {normal: 128, mini: 48}, base_path: base_path, target_file: img, to_file_name: icon_name
    icon_name
  end

  def make_random_file_name(file_name)
    "#{rand(8999999999) + 1000000000}#{File.extname(file_name)}"
  end

  def icon_resize(size_info: {}, base_path: "", target_file: "", to_file_name: "")
    file_name = "#{Rails.root}/tmp/#{to_file_name}"
    File.open(file_name, 'wb') { |of| of.write(target_file.read) }
    img = Magick::ImageList.new file_name
    size_info.each { |key, val| img.resize(val, val).write "#{base_path}/#{key.to_s}/#{to_file_name}" }
  end

end