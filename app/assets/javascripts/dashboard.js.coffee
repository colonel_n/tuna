# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#= require models/message_model
$ ->
  class ViewModel
    constructor: ->
      hash = {}
      $.each $("#group_list").find("a"), (key,value) ->
        hash[$(value).attr("data-id")] = 0
      @tab_manager = hash
    change_tabs: (data,event) =>
      elm = $(event.currentTarget)
      $("#group-name").text elm.attr('data-name')
      $("#group-explain").text elm.attr('data-explain')
      $("#group-icon").attr 'src', "/assets/group_icons/mini/#{elm.attr('data-icon')}"
      data_id = elm.attr('data-id')
      if @tab_manager[data_id] == 0
        new tuna_models.MessageModel().find {group_id:elm.attr('href').replace(/^#gp/,'')}
        @tab_manager[data_id] = 1

  ko.applyBindings( new ViewModel() )

  # 初期処理実行
  target_tab = $('a.js-group-tab:first')
  target_tab.click() if target_tab