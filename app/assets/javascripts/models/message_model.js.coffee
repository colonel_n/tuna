# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# メッセージリソースとの通信クラス
#= require models/model
class MessageModel extends tuna_models.Model
  url: '/messages'

@tuna_models = @tuna_models || {}
@tuna_models.MessageModel = MessageModel








