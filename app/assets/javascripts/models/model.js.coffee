# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
# 通信用の基底クラス
class Model
  url: ''
  find: (conditions) =>
    f = (url) ->
      arr = []
      $.each conditions, (key, value) ->
        arr.push "#{key}=#{value}"
      url + "?" + arr.join '&'

    $.get if conditions then f(@url) else @url

@tuna_models = @tuna_models || {}
@tuna_models.Model = Model








