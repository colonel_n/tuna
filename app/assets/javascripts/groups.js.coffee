# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
$ ->
  class ViewModel
    constructor: ->
      # プレビュー変更
    change_preview: ->
      file = $('#icon_img')[0].files[0]
      return unless file.type.match(/^image\/(png|jpeg|gif|jpg)$/)
      console.log file
      reader = new FileReader()
      reader.addEventListener('load', ->
        document.querySelector("#icon_preview").src = reader.result
      ,true)
      reader.readAsDataURL(file)

  ko.applyBindings( new ViewModel() )