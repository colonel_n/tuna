module GroupsHelper
  def group_icon_img_tag
    image_tag "group_icons/normal/#{@group.icon.presence || 'yjimage.jpeg'}", id: 'icon_preview', size: "128x128"
  end
end
