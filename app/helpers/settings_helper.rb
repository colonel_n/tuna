module SettingsHelper
  def profile_link
    link_to("編集する", @profile.id.present? ? edit_profile_path(@profile.id) : new_profile_path,
            class: "btn btn-primary btn-mini")
  end
end
