module DashboardHelper
  def group_link_tag(group)
    link_to group.name, "#gp#{group.id}", class: "js-group-tab",
            :data => {bind: "event: {click: change_tabs}", toggle: "tab", icon: group.icon, explain: group.explain, name: group.name, id: group.id}
  end
end
