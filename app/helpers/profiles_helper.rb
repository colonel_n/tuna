module ProfilesHelper
  def profile_icon_img_tag
    image_tag "profile_icons/normal/#{@profile.icon.presence || 'agasa.jpg'}", id: 'icon_preview', size: "128x128"
  end
end
